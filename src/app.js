/**
 * Created by ashu on 1/8/16.
 */

import 'babel-polyfill';
import 'bootstrap/dist/css/bootstrap.css'
import * as React from "react";
import ReactDOM from 'react-dom';
import Layout from './components/layout/Layout';

ReactDOM.render(<Layout />, document.getElementById("root"));


