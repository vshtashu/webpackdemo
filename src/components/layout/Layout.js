/**
 * Created by lxg on 19/12/16.
 */

import * as React from "react";
import '../../assets/css/main.css';
import NavBar from '../navbar/Navbar';
import Workspace from '../workspace/Workspace';

class Layout extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="container-fluid padding-none">
                <div className="wrapper col-lg-12 col-md-12 padding-none">
                    <div><NavBar /></div>
                    <div><Workspace /></div>
                </div>
            </div>
        )
    }
}

export default Layout;