/**
 * Created by lxg on 19/12/16.
 */

import * as React from "react";


class Navbar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <aside className="left-panel col-xs-3 col-lg-3 col-md-3 padding-none">
                <header className="left-panel-header">
                    <a href="#">
                        <img className="navbar-brand img-responsive"
                             src={'http://127.0.0.1:3300/src/assets/images/enterprise-bot-logo.png'}/>
                    </a>
                </header>
                <div className="left-panel-menu">
                    <nav className="navbar navbar-default margin-none" role="navigation">
                        <ul className="agent-control margin-none">
                            <li className="current-agent pull-left">
                                <span>Deutsch-Bank-Bot</span>
                            </li>
                            <span className="glyphicon glyphicon-triangle-bottom dropdown-icon pull-right"
                                  aria-hidden="true"> </span>
                            <span className="glyphicon glyphicon-cog settings-icon pull-right"
                                  aria-hidden="true"> </span>
                        </ul>
                        <ul className="agent-dropdown">
                            <li>
                                <span>Deutsch-Bank-Bot</span>
                            </li>
                            <li>
                                <span>RBO-Bank-Bot</span>
                            </li>
                            <span className="vertical-divider margin-none"> </span>
                            <li>
                                <span className="glyphicon glyphicon-plus-sign"> </span><span>Create new agent</span>
                            </li>
                            <li>
                                <span className="glyphicon glyphicon-list"> </span><span>View all agents</span>
                            </li>
                        </ul>
                        <span className="vertical-divider"> </span>
                        <ul className="menu-listing margin-none padding-none">
                            <li className="intents">
                                <img className="intent-icon-sm"
                                     src={'http://127.0.0.1:3300/src/assets/images/intent-icon.png'}/>
                                <span>Intents</span>
                            </li>
                            <li className="entities">
                                <img className="entities-icon-sm"
                                     src={'http://127.0.0.1:3300/src/assets/images/entity-icon.png'}/>
                                <span>Entities</span>
                            </li>
                        </ul>
                        <span className="vertical-divider"> </span>
                    </nav>
                </div>
            </aside>

        )
    }
}

export default Navbar;
