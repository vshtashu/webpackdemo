/**
 * Created by lxg on 19/12/16.
 */


import * as React from "react";
import EntityPage from './Entity/EntityPage'

class Workspace extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <section
                className="dashboard col-xs-9 col-xs-offset-3 col-lg-9 col-lg-offset-3 col-md-9 col-md-offset-3 padding-none">
                <header className="center-header col-xs-9 col-lg-9 col-md-9">
                    <img className="header-icon-lg pull-left"
                         src={'http://127.0.0.1:3300/src/assets/images/intent-icon.png'}/>
                    <h1 className="pull-left margin-none">Entities</h1>
                    <div className="btn-group pull-right">
                        <button type="button" id="create-intent" className="button" data-loading="Working..."
                                data-finished="Done" data-cancelled="Cancelled">Create Entity
                            <span className="tz-bar background-horizontal"> </span>
                            <div className="md-ripple-container"></div>
                        </button>
                        <div className="dropdown pull-right">
                            <div className="dropdown-toggle" type="button" data-toggle="dropdown"> <span
                                className="glyphicon glyphicon-option-vertical"> </span></div>
                            <ul className="dropdown-menu">
                                <li><a>Create Fallback Intent</a></li>
                                <li><a>Upload Intent</a></li>
                            </ul>
                        </div>
                    </div>
                </header>
                <div className="work-area" id="entity-page">
                    <EntityPage />
                </div>
            </section>
        )
    }

}

export default Workspace;