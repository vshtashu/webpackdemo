var webpack = require('webpack');
var path = require("path");

module.exports = {
	devServer: {
		hot: true,
		inline: true,
		port: 3300
	},
	entry: "./src/app.js",
	output: {
		path: path.resolve(__dirname, "/bin/"),
		publicPath: "/bin/",
		filename: "app.bundle.js"
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: "babel-loader"
			},
			{
				test: /\.html$/,
				loader: "html"
			},
			{
				test: /\.css$/,
				loader: "style-loader!css-loader"
			},
			{
				test: /\.json$/,
				loader: 'json-loader'
			}, {
				test: /\.txt$/,
				loader: 'raw-loader'
			}, {
				test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)$/,
				loader: 'url-loader?limit=10000'
			}, {
				test: /\.(eot|ttf|wav|mp3)$/,
				loader: 'file-loader'
			}
		]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin()
	]

};